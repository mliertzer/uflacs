# -*- coding: utf-8 -*-
"""This is uflacs, the UFL Analyser and Compiler System.

Uflacs is currently in an experimental stage, however it is
planned to fairly soon become a useful part of the FEniCS
project, eventually being merged into UFL and/or FFC.
"""

__author__ = "Martin Sandve Alnaes"
__version__ = '1.5.0+'
__date__ = '2015-01-12'
__licence__ = 'LGPL v3'
